#!/bin/bash

(cd centos; ./build.sh)
(cd centos-java; ./build.sh)
(cd haproxy; ./build.sh)
(cd varnish; ./build.sh)
(cd nginx; ./build.sh)
#(cd artifactory; ./build.sh)
#(cd sonar; ./build.sh)
#(cd jenkins-master; ./build.sh)
#(cd jenkins-slave; ./build.sh)
